import pyramid
from cornice.resource import resource, view
from polls.models.student import Student


@resource(collection_path="/students", path="/student/{id}")
class StudentRessource:
    def __init__(self, request):
        self.request = request

    @view(renderer='json')
    def collection_get(self):
        """
        Method to get all students
        :return: exams collection
        """
        return Student.query().all()

    @view(renderer='json')
    def get(self):
        """
        method to get One student
        :return: an exam
        """
        try:
            id_ = int(self.request.matchdict['id'])
        except ValueError:
            raise pyramid.httpexceptions.HTTPNotAcceptable('Id not an integer')
        return Student.get_by_id(id_)

    @view(renderer="json", accept="text/json")
    def collection_post(self):
        """
        Method for adding an Student
        """
        student = Student()
        student.name = self.request.POST['name']
        student.add()
        return True
