from sqlalchemy import engine_from_config
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import  declarative_base
from zope.sqlalchemy import ZopeTransactionExtension

Engine = DBSession = None   # overridden when reading config


def db_session(settings):
    engine = engine_from_config(settings)
    sessionfactory = sessionmaker(
        extension=ZopeTransactionExtension(),
    )
    sessionfactory.configure(bind=engine)
    Base.metadata.bind = engine
    return engine, scoped_session(sessionfactory)


def initdb():
    Base.metadata.create_all(Engine)


def includeme(config):
    global Engine, DBSession
    Engine, DBSession = db_session(config.registry.settings)
    initdb()


class PolBase:
    @classmethod
    def query(cls, *args, **kwargs):
        return DBSession().query(cls, *args, **kwargs)

    def add(self, *args, **kwargs):
        return DBSession().add(self, *args, **kwargs)

    @classmethod
    def get_by_id(cls, id_, *args, **kwargs):
        return cls.query(*args, **kwargs).filter(cls.id == id_).one()


Base = declarative_base(cls=PolBase)
